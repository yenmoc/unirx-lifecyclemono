﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// ReSharper disable ConvertToAutoProperty

// ReSharper disable ArrangeAccessorOwnerBody
// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable VirtualMemberNeverOverridden.Global

// ReSharper disable once CheckNamespace
namespace UniRx
{
    public interface IObservableLifecycleMonoBehaviour
    {
    }

    public interface IObservableAwakeMonoBehaviour : IObservableLifecycleMonoBehaviour
    {
        IObservable<IObservableAwakeMonoBehaviour> OnAwakeAsObservable();
    }

    public interface IObservableStartMonoBehaviour : IObservableLifecycleMonoBehaviour
    {
        IObservable<IObservableStartMonoBehaviour> OnStartAsObservable();
    }

    public abstract class ObservableLifecycleMonoBehaviour : MonoBehaviour, IObservableAwakeMonoBehaviour, IObservableStartMonoBehaviour
    {
        private AsyncSubject<IObservableAwakeMonoBehaviour> _awaken;

        private AsyncSubject<IObservableAwakeMonoBehaviour> Awaken
        {
            get { return _awaken ?? (_awaken = new AsyncSubject<IObservableAwakeMonoBehaviour>()); }
            set { _awaken = value; }
        }

        private AsyncSubject<IObservableStartMonoBehaviour> _started;

        private AsyncSubject<IObservableStartMonoBehaviour> Started
        {
            get { return _started ?? (_started = new AsyncSubject<IObservableStartMonoBehaviour>()); }
            set { _started = value; }
        }

        [SerializeField] private List<GameObject> preAwakeGameObjectList = new List<GameObject>();
        private IEnumerable<GameObject> PreAwakeGameObjectList => preAwakeGameObjectList;

        [SerializeField] private List<ObservableLifecycleMonoBehaviour> preAwakeComponentList = new List<ObservableLifecycleMonoBehaviour>();
        private List<ObservableLifecycleMonoBehaviour> PreAwakeComponentList => preAwakeComponentList;

        [SerializeField] private List<GameObject> preStartGameObjectList = new List<GameObject>();
        private IEnumerable<GameObject> PreStartGameObjectList => preStartGameObjectList;

        [SerializeField] private List<ObservableLifecycleMonoBehaviour> preStartComponentList = new List<ObservableLifecycleMonoBehaviour>();
        private List<ObservableLifecycleMonoBehaviour> PreStartComponentList => preStartComponentList;

        private readonly List<IObservable<IObservableAwakeMonoBehaviour>> _onAwakeObservableList = new List<IObservable<IObservableAwakeMonoBehaviour>>();
        private List<IObservable<IObservableAwakeMonoBehaviour>> OnAwakeObservableList => _onAwakeObservableList;

        private readonly List<IObservable<IObservableStartMonoBehaviour>> _onStartObservableList = new List<IObservable<IObservableStartMonoBehaviour>>();
        private List<IObservable<IObservableStartMonoBehaviour>> OnStartObservableList => _onStartObservableList;

        public IObservable<IObservableAwakeMonoBehaviour> OnAwakeAsObservable()
        {
            return Awaken.AsObservable();
        }

        public IObservable<IObservableStartMonoBehaviour> OnStartAsObservable()
        {
            return Started.AsObservable();
        }

        protected virtual void Awake()
        {
            // Register from all IObservableAwakeMonoBehaviour Components attached to registered GameObject
            PreAwakeGameObjectList.SelectMany(x => x.GetComponents<IObservableAwakeMonoBehaviour>()).ToList().ForEach(x => OnAwakeObservableList.Add(x.OnAwakeAsObservable()));
            // Register from the registered ObservableLifecycleMonoBehaviour Component
            PreAwakeComponentList.ForEach(x => OnAwakeObservableList.Add(x.OnAwakeAsObservable()));
            // Take action when all prefetched MonoBehaviour Awake () calls are complete
            OnAwakeObservableList
                .WhenAll()
                .Subscribe(
                    (_) =>
                    {
                        OnAwake();
                        Awaken.OnNext(this);
                        Awaken.OnCompleted();
                    }
                );
        }

        protected virtual void Start()
        {
            // Register from all IObservableStartMonoBehaviour Components attached to registered GameObject
            PreStartGameObjectList.SelectMany(x => x.GetComponents<IObservableStartMonoBehaviour>()).ToList().ForEach(x => OnStartObservableList.Add(x.OnStartAsObservable()));
            // Register from the registered ObservableLifecycleMonoBehaviour Component
            PreStartComponentList.ForEach(x => OnStartObservableList.Add(x.OnStartAsObservable()));
            // Take action when all lookahead MonoBehaviour Start () calls are complete
            OnStartObservableList
                .WhenAll()
                .Subscribe(
                    (_) =>
                    {
                        OnStart();
                        Started.OnNext(this);
                        Started.OnCompleted();
                    }
                );
        }

        protected virtual void OnDestroy()
        {
            // ReSharper disable once RedundantTypeSpecificationInDefaultExpression
            Awaken = default(AsyncSubject<IObservableAwakeMonoBehaviour>);
            // ReSharper disable once RedundantTypeSpecificationInDefaultExpression
            Started = default(AsyncSubject<IObservableStartMonoBehaviour>);
        }

        protected virtual void OnAwake()
        {
            // TO_DO
        }

        protected virtual void OnStart()
        {
            // TO_DO
        }
    }
}