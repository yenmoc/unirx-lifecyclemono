# UniRx LifeCycleMonobehavior

## Installation

```bash
"com.yenmoc.unirx-lifecyclemono":"https://gitlab.com/yenmoc/unirx-lifecyclemono"
or
npm publish --registry=http://localhost:4873
```

## Usages

### Inherited `ObservableLifecycleMonoBehaviour`

* Inherit classes for waiting for execution order.
* The waiter and the waiter also inherit the `ObservableLifecycleMonoBehaviour`.

### Set GameObject, Component to wait for loading from Inspector

* The following four lists are published as `[SerializeField]`.
   * `Pre Awake GameObject List`
   * `Pre Awake Component List`
   * `Pre Start GameObject List`
   * `Pre Start Component List`
* If you set GameObject, it will wait for all `ObservableLifecycleMonoBehaviour` attached to that GameObject.

### Implement the processing you want to do with Awake (), Start () if necessary

* Implement the processing that you originally wanted to do with Awake () or Start () in the corresponding callback method.
   * `Awake ()`: `void OnAwake ()`
   * `Start ()`: `void OnStart ()`
